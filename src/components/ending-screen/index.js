
// imports
import React from 'react';
import { Link } from 'react-router-dom';

import dbs from './img/dbs.jpg';
import home from './img/home.png';
import replay from './img/replay.png';

import './styles.css';


// Component: slash screen [stateless]
const EndingScreen = (props) => {


    // render
    return (
        <div className="container bg-black bg-black-container ending-screen">
            <div className="text-center valign-center">

                <img src={dbs} className="dbs-logo" alt="DBS Logo"/>
                <div className ="cta-buttons">
                    <Link to={props.home}>
                        <img src={home} alt="home" className="home-btn"/>
                    </Link>
                    <Link to={props.replay}>
                        <img src={replay} alt="replay" className="replay-btn"/>
                    </Link>
                </div>
            </div>
        </div>
    )
};

// export
export default EndingScreen;
